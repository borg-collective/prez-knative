#!/bin/sh
export KUBECONFIG=$PWD/config/k3s.yaml
export PATH=$PWD/kn:$PATH

#kubectl apply -f ping-source.yml

service="knock-knock"
namespace="demo-eventing"
name="test-ping-source"

kn source ping update ${name} \
--namespace ${namespace} \
--data '{"message": "👋 knock-knock", "from": "ping-source"}'