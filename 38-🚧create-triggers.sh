#!/bin/sh
export KUBECONFIG=$PWD/config/k3s.yaml
export PATH=$PWD/kn:$PATH

service="knock-knock"
namespace="demo-eventing"

# Create a trigger 'mytrigger' to declare a subscription to events from default broker. The subscriber is service 'mysvc'
kn trigger create trigger-penny --broker default --namespace ${namespace} --sink svc:${service}-penny
kn trigger create trigger-amy --broker default --namespace ${namespace} --sink svc:${service}-amy

# Ref:
# https://github.com/knative/client/blob/master/docs/cmd/kn_trigger_create.md
