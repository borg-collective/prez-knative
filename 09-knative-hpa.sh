#!/bin/sh
eval $(cat vm.config)
# Optional Serving extensions
multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
# ===========================================================
# Install HPA autoscalling
# ---------------------------------------
# Knative also supports the use of the Kubernetes Horizontal 
# Pod Autoscaler (HPA) for driving autoscaling decisions. 
# The following command will install the components needed 
# to support HPA-class autoscaling:
# ===========================================================

kubectl apply --filename https://github.com/knative/serving/releases/download/v${knative_version}/serving-hpa.yaml
EOF
