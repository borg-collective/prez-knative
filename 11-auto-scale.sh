#!/bin/sh
export KUBECONFIG=$PWD/config/k3s.yaml
export PATH=$PWD/kn:$PATH

# https://knative.dev/docs/serving/samples/autoscale-go/#analysis
# to read: https://knative.dev/docs/serving/configuring-autoscaling/

#                                                       |
#                                  Panic Target--->  +--| 20
#                                                    |  |
#                                                    | <------Panic Window
#                                                    |  |
#       Stable Target--->  +-------------------------|--| 10   CONCURRENCY
#                          |                         |  |
#                          |                      <-----------Stable Window
#                          |                         |  |
#--------------------------+-------------------------+--+ 0
#120                       60                           0
#                     TIME

service="hello-ruby"
namespace="demo"

# source .env && ./create-js-service.sh
# create or update the service
# kn service create --force ${service} \
kn service update ${service} \
--namespace ${namespace} \
--concurrency-target "10" \
--min-scale "1" \
--max-scale "100"
#--image registry.gitlab.com/unimatrix-0/7of9:0.0.1


