#!/bin/sh
eval $(cat vm.config)
ip=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')

service="tada"
namespace="demo"

http POST http://green-${service}.${namespace}.${ip}.xip.io 'name=Bill Ballantine'
http POST http://blue-${service}.${namespace}.${ip}.xip.io 'name=Bob Morane'