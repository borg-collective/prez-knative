#!/bin/sh
export KUBECONFIG=$PWD/config/k3s.yaml
export PATH=$PWD/kn:$PATH

service="hey"
namespace="demo"

kn service update ${service} \
  --namespace ${namespace} \
  --traffic ${service}-yellow="50" \
  --traffic ${service}-orange="50"

kn revision list -s ${service} -n ${namespace}

