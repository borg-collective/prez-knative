#!/bin/sh
export KUBECONFIG=$PWD/config/k3s.yaml

echo "use DashBoard/Manage"

#source .env
kubectl port-forward \
  --namespace knative-monitoring $(kubectl get pods \
  --namespace knative-monitoring \
  --selector=app=grafana  \
  --output=jsonpath="{.items..metadata.name}") 3000
