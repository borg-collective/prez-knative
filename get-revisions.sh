#!/bin/sh
export KUBECONFIG=$PWD/config/k3s.yaml
export PATH=$PWD/kn:$PATH

service=$1
namespace="demo"

kn revision list -s ${service} -n ${namespace}

# https://medium.com/relay-sh/what-is-knative-intro-to-canary-and-blue-green-deployments-with-dashboards-no-yaml-dd0238ff05da
