#!/bin/sh
eval $(cat vm.config)

service="knock-knock"
namespace="demo-eventing"

ip=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')

curl -X POST \
    -H "content-type: application/json"  \
    -H "ce-specversion: 1.0"  \
    -H "ce-source: curl-command"  \
    -H "ce-type: curl.demo"  \
    -H "ce-id: 123-abc"  \
    -d '{"name":"Bob 🎃 Morane 😘👋"}' \
    http://${service}.${namespace}.${ip}.xip.io
