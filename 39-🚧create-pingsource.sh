#!/bin/sh
export KUBECONFIG=$PWD/config/k3s.yaml
export PATH=$PWD/kn:$PATH

service="knock-knock"
namespace="demo-eventing"

kn source ping create my-ping-source --schedule "* * * * *" \
--namespace ${namespace} \
--data '{"message": "✊✊✊", "from": "ping-source"}' \
--sink broker:default
