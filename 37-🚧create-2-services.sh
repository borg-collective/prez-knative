#!/bin/sh
export KUBECONFIG=$PWD/config/k3s.yaml
export PATH=$PWD/kn:$PATH

service="knock-knock"
namespace="demo-eventing"

read -d '' CODE << EOF
let hello = (data) => {
  console.log("👋 Penny, received data", data)

  return {
    response: {
      message: "Hey Penny 🖐️"
    }
  }
}
EOF

kn service create --force ${service}-penny \
  --namespace ${namespace} \
  --env FUNCTION_NAME="hello" \
  --env FUNCTION_CODE="$CODE" \
  --image registry.gitlab.com/borg-collective/borg.queen.wip:latest

read -d '' CODE << EOF
let hello = (data) => {
  console.log("👋 Amy, received data", data)

  return {
    response: {
      message: "Hey Amy 🖐️"
    }
  }
}
EOF

kn service create --force ${service}-amy \
  --namespace ${namespace} \
  --env FUNCTION_NAME="hello" \
  --env FUNCTION_CODE="$CODE" \
  --image registry.gitlab.com/borg-collective/borg.queen.wip:latest


