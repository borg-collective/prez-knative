#!/bin/sh
export KUBECONFIG=$PWD/config/k3s.yaml
export PATH=$PWD/kn:$PATH

service=$1
namespace="demo"

kn route describe ${service} -n ${namespace}