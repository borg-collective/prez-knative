#!/bin/sh

# https://knative.dev/docs/serving/installing-logging-metrics-traces/
# https://knative.dev/docs/install/any-kubernetes-cluster/
# https://knative.dev/docs/install/any-kubernetes-cluster/#installing-the-observability-plugin
# TODO: ELK, Jaeger, Zipkin

eval $(cat vm.config)
multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
# ===========================================================
# Installing the Observability plugin
# ---------------------------------------
# Install the following observability features to enable 
# logging, metrics, and request tracing in your Serving and 
# Eventing components.
# ===========================================================

# ===========================================================
# Oservibility plugins require that 
# you first install the core:
# ===========================================================

kubectl apply --filename https://github.com/knative/serving/releases/download/v${knative_version}/monitoring-core.yaml

# ===========================================================
# Install Prometheus and Grafana for metrics:
# ===========================================================

kubectl apply --filename https://github.com/knative/serving/releases/download/v${knative_version}/monitoring-metrics-prometheus.yaml

EOF





