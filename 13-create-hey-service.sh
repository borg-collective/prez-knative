#!/bin/sh
export KUBECONFIG=$PWD/config/k3s.yaml
export PATH=$PWD/kn:$PATH

service="hey"
namespace="demo"

read -d '' CODE << EOF
function main(params) {
  return {
    message: "👋 hey " + params.getString("name") + " what's up 😃",
  }
}
EOF

# create or update the service
kn service create --force ${service} \
--namespace ${namespace} \
--env FUNCTION_NAME="main" \
--env LANG="js" \
--env FUNCTION_CODE="$CODE" \
--env CONTENT_TYPE="application/json;charset=UTF-8" \
--image registry.gitlab.com/borg-collective/7of9:latest

