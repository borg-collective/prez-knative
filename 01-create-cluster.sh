#!/bin/sh
eval $(cat vm.config)

# Create Ubuntu VM
multipass launch --name ${vm_name} --cpus ${vm_cpus} --mem ${vm_mem} --disk ${vm_disk} \
  --cloud-init ./cloud-init.yaml

# Install K3s
multipass mount config ${vm_name}:config

multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
#curl -sfL https://get.k3s.io | sh -
curl -sfL https://get.k3s.io | sh -s - --disable traefik
EOF

IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')

# Generate config file for kubectl
multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
cat /etc/rancher/k3s/k3s.yaml > config/k3s.yaml
EOF
