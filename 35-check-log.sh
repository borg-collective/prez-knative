#!/bin/sh
export KUBECONFIG=$PWD/config/k3s.yaml

service="knock-knock"
namespace="demo-eventing"

# -l: label
# -c: container
# -n: namespace
watch kubectl logs -l serving.knative.dev/service=${service} -n ${namespace} -c user-container --tail=20

#watch kubectl logs -l serving.knative.dev/service=${service} -n ${namespace} -c user-container

