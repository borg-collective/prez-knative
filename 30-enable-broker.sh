export KUBECONFIG=$PWD/config/k3s.yaml
namespace="demo-eventing"

# The following command enables the default Broker on the default namespace:
kubectl label namespace ${namespace} knative-eventing-injection=enabled

# Verify that the Broker is in a healthy state:
kubectl --namespace ${namespace} get Broker default




