#!/bin/sh
export KUBECONFIG=$PWD/config/k3s.yaml
export PATH=$PWD/kn:$PATH

service="hey"
namespace="demo"

read -d '' CODE << EOF
function main(params) {
  return {
    message: "👋 hey " + params.getString("name") + " what's up 😃",
    revision: "🟡"
  }
}
EOF

# update the service and create a revision
kn service update ${service} \
--namespace ${namespace} \
--env FUNCTION_CODE="$CODE" \
--revision-name yellow

kn revision list -s ${service} -n ${namespace}




