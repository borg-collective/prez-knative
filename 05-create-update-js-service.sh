#!/bin/sh
export KUBECONFIG=$PWD/config/k3s.yaml
export PATH=$PWD/kn:$PATH

service="hello-js"
namespace="demo"

read -d '' CODE << EOF
function hello(params) {
  return {
    message: "😃 Hello World",
    total: 42,
    author: "@k33g_org",
    params: params.getString("name")
  }
}
EOF

# create or update the service
kn service create --force ${service} \
--namespace ${namespace} \
--env FUNCTION_NAME="hello" \
--env LANG="js" \
--env FUNCTION_CODE="$CODE" \
--env CONTENT_TYPE="application/json;charset=UTF-8" \
--image registry.gitlab.com/borg-collective/7of9:latest

