#!/bin/sh
export KUBECONFIG=$PWD/config/k3s.yaml
export PATH=$PWD/kn:$PATH

service="tada"
namespace="demo"

kn service update ${service} \
  --namespace ${namespace} \
  --tag ${service}-green=green

kn revision list -s ${service} -n ${namespace}
echo ""
kn route describe ${service} -n ${namespace}




