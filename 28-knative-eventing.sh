#!/bin/sh
eval $(cat vm.config)
# https://knative.dev/docs/install/any-kubernetes-cluster/
multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF

# Install Knative Eventing

# ===========================================================
# Install the Custom Resource Definitions
# ===========================================================

kubectl apply  --selector knative.dev/crd-install=true \
--filename https://github.com/knative/eventing/releases/download/v${knative_version}/eventing.yaml

# ===========================================================
# Install the core components of Eventing
# ===========================================================

kubectl apply --filename https://github.com/knative/eventing/releases/download/v${knative_version}/eventing.yaml

# urls of previous release
# kubectl apply --filename https://github.com/knative/eventing/releases/download/v${knative_version}/eventing-crds.yaml
# kubectl apply --filename https://github.com/knative/eventing/releases/download/v${knative_version}/eventing-core.yaml

# ===========================================================
# Install a default Channel
# -------------------------
# In-Memory (standalone)
# ===========================================================

# The following command installs an implementation of Channel that runs in-memory. 
# This implementation is nice because it is simple and standalone, 
# 🖐️ but it is unsuitable for production use cases.

kubectl apply --filename https://github.com/knative/eventing/releases/download/v${knative_version}/in-memory-channel.yaml

# ===========================================================
# Install a Broker (eventing) layer
# ---------------------------------
# MT-Channel broker (smaller and simpler installation)
# ===========================================================

# The following command installs an implementation of Broker that utilizes Channels:

kubectl apply --filename https://github.com/knative/eventing/releases/download/v${knative_version}/mt-channel-broker.yaml

# To customize which broker channel implementation is used, 
# update the ConfigMap to specify which configurations are used for which namespaces

kubectl apply --filename ./config-br-mt.yml

# ====== wait ... ======
kubectl wait --for=condition=available deployment/imc-dispatcher -n knative-eventing 
kubectl wait --for=condition=available deployment/imc-controller -n knative-eventing 
kubectl wait --for=condition=available deployment/eventing-webhook -n knative-eventing
kubectl wait --for=condition=available deployment/eventing-controller -n knative-eventing
kubectl wait --for=condition=available deployment/broker-controller -n knative-eventing

kubectl get pods --namespace knative-eventing
EOF





