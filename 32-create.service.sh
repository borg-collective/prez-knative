#!/bin/sh
export KUBECONFIG=$PWD/config/k3s.yaml
export PATH=$PWD/kn:$PATH

service="knock-knock"
namespace="demo-eventing"

# ref https://github.com/cloudevents/sdk-javascript
# data == posted data
# when the service is triggered, it send a new cloud event message
read -d '' CODE << EOF
let hello = (data) => {
  console.log("😃 received data", data)

  return {
    cloudEventData: {
      type: "dev.knative.demo",
      source: "https://gitlab.com/borg-collective/locutus.wip",
      time: new Date(),
      data: data
    },
    response: {
      message: "🖐️ Hey 🖐️",
      received: data
    }
  }
}
EOF

kn service create --force ${service} \
  --namespace ${namespace} \
  --env FUNCTION_NAME="hello" \
  --env FUNCTION_CODE="$CODE" \
  --env WELCOME="Welcome 🎉🎉🎉"\
  --image registry.gitlab.com/borg-collective/locutus.wip:latest 

  