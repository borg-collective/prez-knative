#!/bin/sh
export KUBECONFIG=$PWD/config/k3s.yaml
export PATH=$PWD/kn:$PATH

service="tada"
namespace="demo"

kn service update ${service} \
--namespace ${namespace} \
--traffic ${service}-blue=100 \
--traffic ${service}-green=0

kn revision list -s ${service} -n ${namespace}

