#!/bin/sh
export KUBECONFIG=$PWD/config/k3s.yaml
export PATH=$PWD/kn:$PATH

service="hello-ruby"
namespace="demo"

read -d '' CODE << EOF
def hello_world()
  return "👋 hello world 🌍"
end

def hello(params)
  return hello_world() + " " + params.getString("name")
end
EOF

# create or update the service
kn service create --force ${service} \
--namespace ${namespace} \
--env FUNCTION_NAME="hello" \
--env LANG="ruby" \
--env FUNCTION_CODE="$CODE" \
--env CONTENT_TYPE="plain/text;charset=UTF-8" \
--image registry.gitlab.com/borg-collective/7of9:latest

