#!/bin/sh
export KUBECONFIG=$PWD/config/k3s.yaml
export PATH=$PWD/kn:$PATH

service="tada"
namespace="demo"

kn service update ${service} \
--namespace ${namespace} \
--traffic ${service}-blue=0 \
--traffic ${service}-green=100

kn revision list -s ${service} -n ${namespace}

#kn route describe ${service} -n ${namespace}



